import { createAction, props } from '@ngrx/store';
import { Note } from 'src/app/models/note.model';

const prefix = '[Notes]';

export const getNotes = createAction(`${prefix} Get Notes`);
export const getNotesSuccess = createAction(
  `${getNotes.type} Success`,
  props<{
    notes: Note[];
  }>()
);

export const getNotesToDisplay = createAction(`${prefix} Get Notes To Display`);
export const getNotesToDisplaySuccess = createAction(
  `${getNotesToDisplay.type} Success`,
  props<{
    notesToDisplay: Note[];
  }>()
);

export const getSelectedNotes = createAction(`${prefix} Get Selected Notes`);
export const getSelectedNotesSuccess = createAction(
  `${getSelectedNotes.type} Success`,
  props<{
    selectedNote: Note;
  }>()
);

export const updateNotesToDisplay = createAction(
  `${prefix} Update Notes To Display`,
  props<{
    itemId: number,
    itemType: string
  }>()
);

export const updateNotesToDisplaySuccess = createAction(
  `${updateNotesToDisplay.type} Success`,
  props<{
    notesToDisplay: Note[]
  }>()
);

export const createNote = createAction(
  `${prefix} Create Note`,
  props<{
    note: Note
  }>()
);

export const createNoteSuccess = createAction(
  `${createNote.type} Success`,
  props<{
    note: Note
  }>()
);

export const updateSelectedNote = createAction(
  `${prefix} Update Selectd Note`,
  props<{
    selectedNote: Note
  }>()
);

export const updateSelectedNoteSuccess = createAction(
  `${updateSelectedNote.type} Success`,
  props<{
    selectedNote: Note
  }>()
);

export const updateSelectedNoteById = createAction(
  `${prefix} Update Selected Note By Id`,
  props<{
    noteId: number
  }>()
);

export const updateSelectedNoteByIdSuccess = createAction(
  `${updateSelectedNoteById.type} Success`,
  props<{
    selectedNote: Note
  }>()
);

export const updateNote = createAction(
  `${prefix} Update Note`,
  props<{
    updateNote: Note
  }>()
);

export const updateNoteSuccess = createAction(
  `${updateNote.type} Success`,
  props<{
    updateNote: Note
  }>()
);

export const deleteNote = createAction(
  `${prefix} Delete Note`,
  props<{
    deleteNoteId: number
  }>()
);

export const deleteNoteSuccess = createAction(
  `${deleteNote.type} Success`,
  props<{
    deleteNoteId: number
  }>()
);

export const deleteNoteByNotebookId = createAction(
  `${prefix} Delete Note By Notebook Id`,
  props<{
    deleteNotebookId: number
  }>()
);

export const deleteNoteByNotebookIdSuccess = createAction(
  `${deleteNoteByNotebookId.type} Success`,
  props<{
    deleteNotebookId: number
  }>()
);
