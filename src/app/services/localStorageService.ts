import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class LocalStorageService {
  prefix: string= 'joplin-clone'
  constructor() {}

  public saveData(key: string, value: string) {
    localStorage.setItem(`${this.prefix}-${key}`, value);
  }

  public getData(key: string) {
    return localStorage.getItem(`${this.prefix}-${key}`)
  }

  public removeData(key: string) {
    localStorage.removeItem(`${this.prefix}-${key}`);
  }

  public clearData() {
    localStorage.clear();
  }
}
