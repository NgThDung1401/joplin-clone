import { createReducer, on } from '@ngrx/store';

import { NotebookState } from './notebook.state';
import * as notebookActions from './notebook.actions';
import { LocalStorageService } from 'src/app/services/localStorageService';

const localStorageService = new LocalStorageService()
const notebookFromLS = JSON.parse(localStorageService.getData('notebook'))

export const initialNotebookState: NotebookState = {
  notebooks: (notebookFromLS && notebookFromLS.notebook) ? notebookFromLS.notebook : [],
  selectedNotebookId: 0
};

const reducer = createReducer<NotebookState>(
  initialNotebookState,
  on(notebookActions.getNotebooks, (state) => {
    return {
      ...state
    }
  }),
  on(notebookActions.getNotebooksSuccess, (state, { notebooks }) => {
    return {
      ...state,
      notebooks
    };
  }),

  on(notebookActions.getSelectedNotebookId, (state) => {
    return {
      ...state
    }
  }),
  on(notebookActions.getSelectedNotebookIdSuccess, (state, { selectedNotebookId }) => {
    return {
      ...state,
      selectedNotebookId
    };
  }),

  on(notebookActions.updateSelectedNotebookId, (state) => {
    return {
      ...state,
    };
  }),
  on(notebookActions.updateSelectedNotebookIdSuccess, (state, { selectedNotebookId }) => {
    return {
      ...state,
      selectedNotebookId,
    };
  }),

  on(notebookActions.createNotebook, (state) => {
    return {
      ...state,
    };
  }),
  on(notebookActions.createNotebookSuccess, (state, { notebook }) => {
    return {
      ...state,
      notebooks: [...state.notebooks, notebook],
    };
  }),

  on(notebookActions.deleteNotebook, (state) => {
    return {
      ...state,
    };
  }),
  on(notebookActions.deleteNotebookSuccess, (state, { notebook }) => {
    return {
      ...state,
      notebooks: state.notebooks.filter((b) => b.id !== notebook.id),
    };
  }),
)

export function notebookReducer(state = initialNotebookState, actions: any): NotebookState {
  return reducer(state, actions)
}
