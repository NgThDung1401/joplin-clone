import { ComponentFixture, TestBed, fakeAsync, flush } from '@angular/core/testing';

import { SidenavComponent } from './sidenav.component';
import { AppModule } from 'src/app/app.module';

import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzSubMenuComponent } from 'ng-zorro-antd/menu';
import { IconDefinition } from '@ant-design/icons-angular';
import {TagsOutline, BookOutline, PlusOutline, TagOutline} from '@ant-design/icons-angular/icons';
import { By } from '@angular/platform-browser';
const icons: IconDefinition[] = [TagsOutline, BookOutline, PlusOutline, TagOutline]


const mockNotebooks = [
  {
    id: 1,
    name: 'Notebook 1',
  },
  {
    id: 2,
    name: 'Notebook 2',
  },
  {
    id: 3,
    name: 'Notebook 3',
  },
]
const mockTags = [
  {
    id: 1,
    label: 'Tag 1'
  },
  {
    id: 2,
    label: 'Tag 2'
  },
  {
    id: 3,
    label: 'Tag 3'
  },
]

describe('SidenavComponent', () => {
  let component: SidenavComponent;
  let fixture: ComponentFixture<SidenavComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        AppModule,
        NzIconModule.forChild(icons),
        NzMenuModule,
      ],
      declarations: [
        SidenavComponent,
        NzSubMenuComponent,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(SidenavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the sidenav component', () => {
    expect(component).toBeTruthy();
  });

  it('changeSelectedNotebookId() should has been called when notebook in navigate is clicked', fakeAsync(() => {
    component = fixture.componentInstance;
    component.notebooks = mockNotebooks;
    fixture.detectChanges();

    const changeSelectedNotebookIdSpy = spyOn(component, 'changeSelectedNotebookId');
    const notebookItems = fixture.debugElement.queryAll(By.css('li.notebook-item'));

    // click "All notes"
    notebookItems[0].nativeElement.click();
    flush()
    expect(changeSelectedNotebookIdSpy).toHaveBeenCalledWith(0);

    // click Notebook item
    mockNotebooks.forEach((notebook, index) => {
      notebookItems[index + 1].nativeElement.click();
      flush();
      expect(changeSelectedNotebookIdSpy).toHaveBeenCalledWith(notebook.id);
    });
  }))

  it('changeSelectedTagId() should has been called when tag in navigate is clicked', fakeAsync(() => {
    component = fixture.componentInstance;
    component.tags = mockTags;
    fixture.detectChanges();

    const changeSelectedTagIdSpy = spyOn(component, 'changeSelectedTagId');
    const tagItems = fixture.debugElement.queryAll(By.css('li.tag-item'));

    mockTags.forEach((tag, index) => {
      tagItems[index].nativeElement.click();
      flush();
      expect(changeSelectedTagIdSpy).toHaveBeenCalledWith(tag.id);
    });
  }))

  it('showModal() should has been called when plus icon in navigate is clicked', fakeAsync(() => {
    component = fixture.componentInstance;
    fixture.detectChanges();

    const showModalSpy = spyOn(component, 'showModal');
    const plusIcon = fixture.debugElement.query(By.css('.new-notebook-icon'));

    plusIcon.nativeElement.click();
    flush();
    expect(showModalSpy).toHaveBeenCalled();
  }))

  it('handleOk() should has been called when "OK" button in "Create notebook" modal is clicked', fakeAsync(() => {
    component = fixture.componentInstance;
    component.isVisible = true;
    fixture.detectChanges();

    const handleOkSpy = spyOn(component, 'handleOk');
    const okButton = fixture.debugElement.query(By.css('button.new-notebook-create'));

    okButton.nativeElement.click();
    flush();
    expect(handleOkSpy).toHaveBeenCalled();
  }))

  it('handleCancel() should has been called when "Cancel" button in "Create notebook" modal is clicked', fakeAsync(() => {
    component = fixture.componentInstance;
    component.isVisible = true;
    fixture.detectChanges();

    const handleCancelSpy = spyOn(component, 'handleCancel');
    const cancelButton = fixture.debugElement.query(By.css('button.new-notebook-cancel'));

    cancelButton.nativeElement.click();
    flush();
    expect(handleCancelSpy).toHaveBeenCalled();
  }))
});
