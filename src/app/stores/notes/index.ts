import { NgModule } from '@angular/core';

import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { NotesEffects } from './note.effects';
import { noteReducer } from './note.reducers';

@NgModule({
  imports: [
      StoreModule.forFeature('note', noteReducer),
      EffectsModule.forFeature([NotesEffects])
  ]
})
export class NoteStoreModule {}
