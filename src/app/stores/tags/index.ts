import { NgModule } from '@angular/core';

import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { TagsEffects } from './tag.effects';
import { tagReducer } from './tag.reducers';

@NgModule({
  imports: [
      StoreModule.forFeature('tag', tagReducer),
      EffectsModule.forFeature([TagsEffects])
  ]
})
export class TagStoreModule {}
