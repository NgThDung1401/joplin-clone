import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotebookComponent } from './components/notebook/notebook.component';
import { NoteEditorComponent } from './components/note-editor/note-editor.component';

const routes: Routes = [
  { path: '', redirectTo: '/notebook/0', pathMatch: 'full' },
  { path: 'notebook/:id',
    component:  NotebookComponent,
    children: [
      { path: 'note/:id', component: NoteEditorComponent}
    ]
  },
  { path: 'tag/:id',
    component:  NotebookComponent,
    children: [
      { path: 'note/:id', component: NoteEditorComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
