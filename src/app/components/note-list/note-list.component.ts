import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Note } from 'src/app/models/note.model';
import { Store } from '@ngrx/store';
import * as noteActions from 'src/app/stores/notes/note.actions'
import * as tagActions from 'src/app/stores/tags/tag.actions'
import { NzContextMenuService, NzDropdownMenuComponent } from 'ng-zorro-antd/dropdown';
import { NzModalService } from 'ng-zorro-antd/modal';

import { Router } from '@angular/router';

@Component({
  selector: 'app-note-list',
  templateUrl: './note-list.component.html',
  styleUrls: ['./note-list.component.css']
})
export class NoteListComponent  implements OnInit {
  @Input() notesToDisplay: Note[];
  @Input() selectedNote: Note;
  @Input() selectedNotebookId: number;
  @Input() selectedTagId: number;

  @Output() searchNoteEvent = new EventEmitter<string>();

  searchValue:string = ''

  ngOnInit(): void {
    this.initDispatch();
  }

  constructor(
    private readonly store: Store,
    private nzContextMenuService: NzContextMenuService,
    private modal: NzModalService,
    private router: Router) {}

  private initDispatch(): void {
    this.store.dispatch(noteActions.getNotesToDisplay());
    this.store.dispatch(noteActions.getSelectedNotes());
    this.store.dispatch(noteActions.getNotes());
  }

  public changeSelectedNote(note: Note): void {
    this.store.dispatch(tagActions.getTagIdsByNoteId({noteId: note.id}))
    if ((this.selectedNotebookId === 0) && this.searchValue === '') {
      this.router.navigate([`/notebook/${this.selectedNotebookId}/note/${note.id}`]);
    } else if (this.selectedTagId && this.selectedTagId !== 0) {
      this.router.navigate([`/tag/${this.selectedTagId}/note/${note.id}`]);
    } else {
      this.router.navigate([`/notebook/${note.notebookId}/note/${note.id}`]);
    }
  }

  public turnOnCreatingMode(): void {
    const newNote = {
      id: 0,
      notebookId: 0,
      title: "",
      content: "",
    }
    this.store.dispatch(noteActions.updateSelectedNote({selectedNote: newNote}))
  }

  public contextMenu($event: MouseEvent, menu: NzDropdownMenuComponent) {
    this.nzContextMenuService.create($event, menu);
  }

  public showConfirm(note: Note) {
    this.modal.confirm({
      nzTitle: `Delete note "${note.title}"?`,
      nzContent: '',
      nzOkText: 'OK',
      nzOnOk: () => this.deleteNote(note.id),
      nzCancelText: 'Cancel',
      nzOnCancel: () => {}
    });
  }

  public deleteNote(id: number) {
    this.store.dispatch(noteActions.deleteNote({deleteNoteId: id}))
  }

  public checkIsNullTitle(title: string): boolean {
    if (!title || title.trim() === '') {
      return true
    } else {
      return false
    }
  }

  public searchNote() {
    this.searchNoteEvent.emit(this.searchValue)
  }

  public clearSearchValue() {
    this.searchValue = ''
    this.searchNoteEvent.emit('')
  }
}
