import { createAction, props } from '@ngrx/store';
import { Tag } from 'src/app/models/tag.model';

const prefix = '[Tags]';

export const getTags = createAction(`${prefix} Get Tags`);
export const getTagsSuccess = createAction(
  `${getTags.type} Success`,
  props<{
    tags: Tag[];
  }>()
);

export const getTagIdsByNoteId = createAction(
  `${prefix} Get Tag Id List By Note Id`,
  props<{
    noteId: number
  }>()
  );
export const getTagIdsByNoteIdSuccess = createAction(
  `${getTagIdsByNoteId.type} Success`,
  props<{
    tagIds: number[];
  }>()
);

export const createTag = createAction(
  `${prefix} Create Tag`,
  props<{
    tag: Tag,
    noteId: number
  }>()
);

export const createTagSuccess = createAction(
  `${createTag.type} Success`,
  props<{
    tag: Tag
  }>()
);

export const deleteTag = createAction(
  `${prefix} Delete Tag`,
  props<{
    deleteTagId: number
  }>()
);

export const deleteTagSuccess = createAction(
  `${deleteTag.type} Success`,
  props<{
    deleteTagId: number
  }>()
);

export const updateTagListOfNote = createAction(
  `${prefix} Update TagList of Note`,
  props<{
    tagIds: number[],
    noteId: number
  }>()
);

export const updateTagListOfNoteSuccess = createAction(
  `${updateTagListOfNote.type} Success`,
  props<{
    tagNotes: {tagId: number, noteId: number}[]
    tags: Tag[]
  }>()
);

export const updateSelectedTagId = createAction(
  `${prefix} Update Selected Tag Id`,
  props<{
    selectedTagId: number
  }>()
);

export const updateSelectedTagIdSuccess = createAction(
  `${updateSelectedTagId.type} Success`,
  props<{
    selectedTagId: number
  }>()
);
