import { NgModule } from "@angular/core";
import { NotebookStoreModule } from "./notebooks";
import { NoteStoreModule } from "./notes";
import { TagStoreModule } from "./tags";

@NgModule({
  declarations: [],
  imports: [
    NotebookStoreModule,
    NoteStoreModule,
    TagStoreModule,
  ]
})
export class MyStoreModule {}
