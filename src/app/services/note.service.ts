import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Note } from '../models/note.model';
import { LocalStorageService } from './localStorageService';

@Injectable({
    providedIn: 'root'
})
export class NoteService {

  private notes: Note[] = [];

  private notesToDisplay: Note[];

  private selectedNote: Note;

  private tagNotes: {tagId: number, noteId: number}[];

  constructor(private readonly localStorageService: LocalStorageService) {
    const noteFromLS = JSON.parse(localStorageService.getData('note'))
    const tagNoteFromLS = JSON.parse(localStorageService.getData('tag_note'))

    if (noteFromLS && noteFromLS.note) {
      this.notes = noteFromLS.note
      this.notesToDisplay = noteFromLS.note
      if (noteFromLS.note.length > 0) {
        this.selectedNote = noteFromLS.note[0]
      } else {
        this.selectedNote = {
          id: 0,
          notebookId: 0,
          title: '',
          content: ''
        }
      }
    }

    if (tagNoteFromLS && tagNoteFromLS.tagNote) {
      this.tagNotes= tagNoteFromLS.tagNote
    }
  }

  getNotes(): Observable<Note[]> {
    return of(this.notes);
  }

  getNotesToDisplay(): Observable<Note[]> {
    return of(this.notesToDisplay);
  }

  getSelectedNote():Observable<Note> {
    return of(this.selectedNote)
  }

  updateNotesToDisplay(itemId: number, itemType: string): Observable<Note[]> {
    if (itemType === 'notebook') {
      if (itemId === 0) {
        this.notesToDisplay = this.notes.map(note => note)
      } else {
        this.notesToDisplay = this.notes.filter(note => note.notebookId === itemId);
      }
    }
    else if (itemType === 'tag') {
      this.tagNotes = JSON.parse(this.localStorageService.getData('tag_note')).tagNote
      let noteIds = this.tagNotes
      .filter(tagNote => tagNote.tagId === itemId)
      .map(tagNote => tagNote.noteId)

      this.notesToDisplay = []

      noteIds.forEach(noteId => {
        this.notesToDisplay = this.notesToDisplay.concat(this.notes.filter(note => note.id === noteId))
      });
    }

    return of(this.notesToDisplay);
  }

  updateSelectedNote(note: Note): Observable<Note> {
    this.selectedNote = {
      ...note
    }
    return of(this.selectedNote)
  }

  updateSelectedNoteById(noteId: number): Observable<Note> {
    if (noteId !== 0) {
      let foundNoteList = this.notes.filter(note => note.id === noteId)
      if (foundNoteList.length > 0) {
        this.selectedNote = {
          ...foundNoteList[0]
        }
      }
    } else {
      this.selectedNote = {
        id: 0,
        notebookId: 0,
        title: '',
        content: '',
      }
    }

    return of(this.selectedNote)
  }

  create(note: Note): Observable<Note> {
    let newNoteId = 1;

    if (this.notes.length > 0) {
      newNoteId = this.notes[this.notes.length - 1].id + 1
    }

    let newNote = {
      id: newNoteId,
      notebookId: note.notebookId,
      title: note.title,
      content: note.content,
      createdAt: note.createdAt,
      updatedAt: note.updatedAt
    }

    this.notes = [
      ...this.notes,
      newNote
    ];

    this.notesToDisplay = [
      ...this.notesToDisplay,
      newNote
    ]

    this.selectedNote = newNote

    this.localStorageService.saveData('note', JSON.stringify({note: this.notes}))

    return of(newNote);
  }

  update(updateNote: Note): Observable<Note> {
    let noteData = {
      id: this.selectedNote.id,
      notebookId: this.selectedNote.notebookId,
      title: updateNote.title,
      content: updateNote.content,
      createdAt: this.selectedNote.createdAt,
      updatedAt: updateNote.updatedAt
    }

    this.notes = this.notes.map(note => note.id === noteData.id ? noteData : note)
    this.notesToDisplay = this.notesToDisplay.map(note => note.id === noteData.id ? noteData : note)

    this.localStorageService.saveData('note', JSON.stringify({note: this.notes}))

    return of(noteData)
  }

  delete(deleteNoteId: number): Observable<Number> {

    this.notes = this.notes.filter(note => note.id !== deleteNoteId);
    this.notesToDisplay = this.notesToDisplay.filter(note => note.id !== deleteNoteId);

    this.localStorageService.saveData('note', JSON.stringify({note: this.notes}))

    return of(deleteNoteId)
  }

  deleteByNotebookId(deleteNotebookId: number): Observable<Number> {

    this.notes = this.notes.filter(note => note.notebookId !== deleteNotebookId);

    this.localStorageService.saveData('note', JSON.stringify({note: this.notes}))

    return of(deleteNotebookId)
  }
}
