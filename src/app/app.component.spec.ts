import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { AppComponent } from './app.component';
import { SidenavComponent } from './components/sidenav/sidenav.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { MyStoreModule } from './stores';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { IconDefinition } from '@ant-design/icons-angular';

import {TagsOutline, BookOutline, PlusOutline, TagOutline} from '@ant-design/icons-angular/icons';
const icons: IconDefinition[] = [TagsOutline, BookOutline, PlusOutline, TagOutline]

describe('AppComponent', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      RouterTestingModule.withRoutes([]),
      BrowserAnimationsModule,
      NzMenuModule,
      NzButtonModule,
      NzInputModule,
      NzIconModule.forChild(icons),
      NzModalModule,
      NzDropDownModule,
      NzSelectModule,
      NzGridModule,
      StoreModule.forRoot({}, {}),
      EffectsModule.forRoot(),
      StoreDevtoolsModule.instrument({}),
      MyStoreModule,
    ],
    declarations: [
      AppComponent,
      SidenavComponent,
    ]
  }).compileComponents());

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'joplin-clone'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('joplin-clone');
  });
});
