import { createFeatureSelector, createSelector } from '@ngrx/store';

import { NotebookState } from './notebook.state';

export const selectNotebookState = createFeatureSelector<NotebookState>('notebook');
export const selectNotebooksList = createSelector(selectNotebookState, (state) => state.notebooks);
export const selectSelectedNotebookId = createSelector(selectNotebookState, (state) => state.selectedNotebookId);
