import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, switchMap } from 'rxjs/operators';
import * as notebookActions from './notebook.actions';
import { NotebookService } from 'src/app/services/notebook.service';
import { Notebook } from 'src/app/models/notebook.model';

@Injectable()
export class NotebooksEffects {
  constructor(private readonly actions$: Actions, private readonly notebookService: NotebookService) {}

  getNotebooks$ = createEffect(() =>
    this.actions$.pipe(
      ofType(notebookActions.getNotebooks.type),
      switchMap(() => this.notebookService.getNotebooks()),
      map((notebooks: Notebook[]) => notebookActions.getNotebooksSuccess({notebooks}))
    )
  );

  getSelectedNotebookId$ = createEffect(() =>
    this.actions$.pipe(
      ofType(notebookActions.getSelectedNotebookId.type),
      switchMap(() => this.notebookService.getSelectedNotebookId()),
      map((selectedNotebookId: number) => notebookActions.getSelectedNotebookIdSuccess({selectedNotebookId}))
    )
  );

  updateSelectedNotebookId$ = createEffect(() =>
    this.actions$.pipe(
      ofType(notebookActions.updateSelectedNotebookId),
      switchMap(({selectedNotebookId}) => this.notebookService.updateSelectedNotebookId(selectedNotebookId)),
      map((selectedNotebookId: number) => notebookActions.updateSelectedNotebookIdSuccess({selectedNotebookId}))
    )
  );

  createNotebook$ = createEffect(() =>
    this.actions$.pipe(
      ofType(notebookActions.createNotebook),
      switchMap(({notebook}) => this.notebookService.create(notebook)),
      map((notebook: Notebook) => notebookActions.createNotebookSuccess({notebook}))
    )
  );

  deleteNotebook$ = createEffect(() =>
    this.actions$.pipe(
      ofType(notebookActions.deleteNotebook),
      switchMap(({notebook}) => this.notebookService.delete(notebook)),
      map((notebook: Notebook) => notebookActions.deleteNotebookSuccess({notebook}))
    )
  );
}
