import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, switchMap } from 'rxjs/operators';
import * as tagActions from './tag.actions';
import { TagService } from 'src/app/services/tag.service';
import { Tag } from 'src/app/models/tag.model';

@Injectable()
export class TagsEffects {
  constructor(private readonly actions$: Actions, private readonly tagService: TagService) {}

  getTags$ = createEffect(() =>
    this.actions$.pipe(
      ofType(tagActions.getTags.type),
      switchMap(() => this.tagService.getTags()),
      map((tags: Tag[]) => tagActions.getTagsSuccess({tags}))
    )
  );

  getTagIdListByNoteId$ = createEffect(() =>
    this.actions$.pipe(
      ofType(tagActions.getTagIdsByNoteId.type),
      switchMap(({noteId}) => this.tagService.getTagIdListByNoteId(noteId)),
      map((tagIds: number[]) => tagActions.getTagIdsByNoteIdSuccess({tagIds}))
    )
  );

  createTag$ = createEffect(() =>
    this.actions$.pipe(
      ofType(tagActions.createTag),
      switchMap(({tag, noteId}) => this.tagService.create(tag, noteId)),
      map((tag: Tag) => tagActions.createTagSuccess({tag}))
    )
  );

  deleteTag$ = createEffect(() =>
    this.actions$.pipe(
      ofType(tagActions.deleteTag),
      switchMap(({deleteTagId}) => this.tagService.delete(deleteTagId)),
      map((deleteTagId: number) => tagActions.deleteTagSuccess({deleteTagId}))
    )
  );

  updateTagListOfNote$ = createEffect(() =>
    this.actions$.pipe(
      ofType(tagActions.updateTagListOfNote),
      switchMap(({tagIds, noteId}) => this.tagService.updateTagListOfNote(tagIds, noteId)),
      map((returnObject: {tagNotes: {tagId: number, noteId: number}[], tags: Tag[]}) => tagActions.updateTagListOfNoteSuccess(returnObject))
    )
  );

  updateSelectedTag$ = createEffect(() =>
    this.actions$.pipe(
      ofType(tagActions.updateSelectedTagId),
      switchMap(({selectedTagId}) => this.tagService.updateSelectedTagId(selectedTagId)),
      map((selectedTagId: number) => tagActions.updateSelectedTagIdSuccess({selectedTagId}))
    )
  );
}
