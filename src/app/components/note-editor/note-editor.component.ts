import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Note } from 'src/app/models/note.model';
import { Tag } from 'src/app/models/tag.model';
import { select, Store } from '@ngrx/store';
import * as noteSelectors from 'src/app/stores/notes/note.selectors'
import * as noteActions from 'src/app/stores/notes/note.actions'
import * as notebookActions from 'src/app/stores/notebooks/notebook.actions'
import * as notebookSelectors from 'src/app/stores/notebooks/notebook.selectors'
import * as tagSelectors from 'src/app/stores/tags/tag.selectors'
import * as tagActions from 'src/app/stores/tags/tag.actions'
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-note-editor',
  templateUrl: './note-editor.component.html',
  styleUrls: ['./note-editor.component.css']
})
export class NoteEditorComponent implements OnInit {
  @ViewChild('editNoteTitleInput') editNoteTitleInputEle: ElementRef;
  selectedNote: Note
  editingNote: Note = {
    id: 0,
    notebookId: 0,
    title: "",
    content: "",
  }
  selectedNotebookId: number = 0;

  isVisible:boolean = false;
  isNoteInfoVisible: boolean = false;

  listOfOption: Array<{
    value: number,
    label: string,
    isCreating: boolean
  }> = [];
  listOfSelectedValue = [];

  listOfSelectedTag: Tag[] = [];

  newTag: any = {
    value: 0,
    label: '',
  }

  selectedTagId: number = 0
  tags: Tag[] = []

  ngOnInit(): void {
    this.updateSelectedNoteById(Number(this.route.snapshot.params['id']));
    this.initDispatch();
    this.initSubscriptions();
  }

  constructor(
    private readonly store: Store,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location) {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        const eventUrlSplited = event.url.split('/');
        const selectedNoteId = Number(eventUrlSplited[4])
        if (selectedNoteId || selectedNoteId === 0) {
          this.updateSelectedNoteById(selectedNoteId);
          this.store.dispatch(tagActions.getTagIdsByNoteId({noteId: selectedNoteId}))
        }

        if (eventUrlSplited[1] === 'tag') {
          this.selectedTagId = Number(eventUrlSplited[2])
        }
      }
    });
  }

  private initDispatch(): void {
    this.store.dispatch(noteActions.getSelectedNotes());
    this.store.dispatch(notebookActions.getSelectedNotebookId());
    this.store.dispatch(tagActions.getTagIdsByNoteId({noteId: Number(this.route.snapshot.params['id'])}))
  }

  private initSubscriptions(): void {
    this.store
    .pipe(select(noteSelectors.selectSelectedNote))
    .subscribe(value => {
      this.selectedNote = value
      this.editingNote = {
        ...this.selectedNote
      }
    });

    this.store
    .pipe(select(notebookSelectors.selectSelectedNotebookId))
    .subscribe(value => this.selectedNotebookId = value);

    this.store
    .pipe(select(tagSelectors.selectTagsList))
    .subscribe(value => {
      this.listOfOption = value.map(tag => ({
        value: tag.id,
        label: tag.label,
        isCreating: false
      }))
    });

    this.store
    .pipe(select(tagSelectors.selectTagIdsByNoteId))
    .subscribe(value => {
      this.listOfSelectedValue = value;
      this.getListOfSelectedTag();
    });

    this.store
    .pipe(select(tagSelectors.selectTagsList))
    .subscribe(value => {
      this.tags = value;
    });
  }

  private updateSelectedNoteById(selectedNoteId: number) {
    if (selectedNoteId) {
      this.store.dispatch(noteActions.updateSelectedNoteById({noteId: selectedNoteId}))
    } else if (selectedNoteId === 0) {
      this.store.dispatch(noteActions.updateSelectedNoteById({noteId: 0}))
    }
  }

  public createNote() {
    let newNote = {
      id: 0,
      notebookId: this.selectedNotebookId,
      title: this.editingNote.title,
      content: this.editingNote.content,
      createdAt: Date.now(),
      updatedAt: Date.now()
    }
    this.store.dispatch(noteActions.createNote({note: newNote}))
    this.router.navigate([`/notebook/${this.selectedNotebookId}/note/${this.selectedNote.id}`]);
    setTimeout(() => {
      this.editNoteTitleInputEle.nativeElement.focus();
    }, 0);
  }

  public updateNote() {
    let editingNote = {
      id: 0,
      notebookId: 0,
      title: this.editingNote.title,
      content: this.editingNote.content,
      updatedAt: Date.now()
    }
    this.store.dispatch(noteActions.updateNote({updateNote: editingNote}))
  }

  public showTagModal(): void {
    this.isVisible = true;
  }

  public handleOkTagModal(): void {
    this.isVisible = false;
    this.createTag();
    this.updateTagListOfNote();
  }

  public handleCancelTagModal(): void {
    this.isVisible = false;
  }

  public tagOnChange($event: string) {
    this.newTag.label = $event;
  }

  public handleTagSelectChange($event: any[]) {
    if ($event[$event.length - 1] === 0) {
        this.listOfSelectedValue = this.listOfSelectedValue.filter(value => value !== 0);
        let newTag = {
          value: this.listOfOption.length + 1,
          label: this.newTag.label,
          isCreating: true
        }

        this.listOfSelectedValue.push(newTag.value)
        this.listOfOption.push(newTag)
      }
  }

  public createTag() {
    this.listOfOption.forEach(option => {
      if (option.isCreating) {
        this.store.dispatch(tagActions.createTag({
          tag: {
            id: option.value,
            label: option.label
          },
          noteId: this.selectedNote.id
        }))
      }
    });
  }

  public updateTagListOfNote() {
    this.store.dispatch(tagActions.updateTagListOfNote({
      tagIds: this.listOfSelectedValue,
      noteId: this.selectedNote.id
    }))
    this.getListOfSelectedTag();
    if (this.tags.filter(tag => tag.id === this.selectedTagId).length === 0
        || this.listOfSelectedValue.filter(value => value === this.selectedTagId).length === 0
    ) {
      this.router.navigate([`/notebook/${this.selectedNote.notebookId}/note/${this.selectedNote.id}`]);
    }
  }

  public back() {
    this.location.back();
  }

  public forward() {
    this.location.forward()
  }

  public getListOfSelectedTag() {
    this.listOfSelectedTag = []
    this.listOfSelectedValue.forEach(value => {
      this.listOfSelectedTag = this.listOfSelectedTag.concat(
        this.listOfOption
        .filter(option => option.value === value)
        .map(option => ({
          id: option.value,
          label: option.label
        }))
      )
    });
  }

  public clickTagOnFooter(tag: Tag) {
    this.store.dispatch(tagActions.updateSelectedTagId({selectedTagId: tag.id}))
    this.store.dispatch(notebookActions.updateSelectedNotebookId({selectedNotebookId: 0}))
    this.store.dispatch(noteActions.updateNotesToDisplay({itemId: tag.id, itemType: 'tag'}))
    this.router.navigate([`/tag/${tag.id}/note/${this.selectedNote.id}`]);
  }

  public showNoteInfoModal() {
    this.isNoteInfoVisible = true;
  }

  public handleCancelNoteInfoModal() {
    this.isNoteInfoVisible = false;
  }

  public convertUnixToDate(unixTime: number): string {
    let date = new Date(unixTime);
    return date.toLocaleDateString('en-US', {year: "numeric", month: "numeric", day: "numeric"})
    + ' ' + date.getHours()
    + ':' + date.getMinutes()
    + ':' + date.getSeconds();
  }

  public writeToClipboard(text: string) {
    navigator.clipboard.writeText(text);
  }
}
