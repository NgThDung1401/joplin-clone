import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoteEditorComponent } from './note-editor.component';

import { AppModule } from 'src/app/app.module';
import { ActivatedRoute } from '@angular/router';

const activatedRouteMock = {
  snapshot: {
    params: {
      id: '1'
    }
  }
}

describe('NoteEditorComponent', () => {
  let component: NoteEditorComponent;
  let fixture: ComponentFixture<NoteEditorComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        AppModule,
      ],
      declarations: [NoteEditorComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: activatedRouteMock
        }
      ]
    });
    fixture = TestBed.createComponent(NoteEditorComponent);
    component = fixture.componentInstance;
    component.selectedNote = {
      id: 1,
      notebookId: 1,
      title: 'Note 1',
      content: 'Note 1',
    }

    component.editingNote = {
      id: 1,
      notebookId: 1,
      title: 'Note 1',
      content: 'Note 1',
    }

    component.tags = [
      {
        id: 1,
        label: 'Tag 1',
      }
    ]
    fixture.detectChanges();
  });

  it('should create the note editor component', () => {
    expect(component).toBeTruthy();
  });
});
