import { Tag } from "src/app/models/tag.model"

export interface TagState {
  tags: Tag[],
  tagIdsByNoteId: number[],
  selectedTagId: number,
}
