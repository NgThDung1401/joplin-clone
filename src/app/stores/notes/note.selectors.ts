import { createFeatureSelector, createSelector } from '@ngrx/store';

import { NoteState } from './note.state';

export const selectNoteState = createFeatureSelector<NoteState>('note');
export const selectNotesList = createSelector(selectNoteState, (state) => state.notes);
export const selectNotesToDisplayList = createSelector(selectNoteState, (state) => state.notesToDisplay);
export const selectSelectedNote = createSelector(selectNoteState, (state) => state.selectedNote);
