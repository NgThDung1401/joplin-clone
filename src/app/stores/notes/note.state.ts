import { Note } from "src/app/models/note.model"

export interface NoteState {
  notes: Note[],
  notesToDisplay: Note[],
  selectedNote: Note,
}
