import { Injectable } from '@angular/core';
import { Tag } from '../models/tag.model';
import { Observable, of } from 'rxjs';
import { LocalStorageService } from './localStorageService';

@Injectable({
    providedIn: 'root'
})
export class TagService {

  private tags: Tag[] = []
  private tagNotes: {tagId: number, noteId: number}[] = []
  private selectedTagId: number = 0

  constructor(private readonly localStorageService: LocalStorageService) {
    const tagFromLS = JSON.parse(localStorageService.getData('tag'))
    const tagNoteFromLS = JSON.parse(localStorageService.getData('tag_note'))
    if (tagFromLS && tagFromLS.tag) {
      this.tags = tagFromLS.tag
    }
    if (tagNoteFromLS && tagNoteFromLS.tagNote) {
      this.tagNotes = tagNoteFromLS.tagNote
    }
  }

  getTags(): Observable<Tag[]> {
    return of(this.tags);
  }

  getTagIdListByNoteId(noteId: number): Observable<number[]> {
    let tagIdListByNoteId = this.tagNotes
    .filter(tagNote => tagNote.noteId === noteId)
    .map(tagNote => tagNote.tagId)

    return of(tagIdListByNoteId)
  }

  create(tag: Tag, noteId: number): Observable<Tag> {
    this.tags = [
      ...this.tags,
      tag
    ];

    let newTagNote = {
      tagId: tag.id,
      noteId
    }

    this.tagNotes = [
      ...this.tagNotes,
      newTagNote
    ]

    this.localStorageService.saveData('tag', JSON.stringify({tag: this.tags}))
    this.localStorageService.saveData('tag_note', JSON.stringify({tagNote: this.tagNotes}))

    return of(tag);
  }

  delete(tagId: number): Observable<number> {
    this.tags = this.tags.filter(tag => tag.id !== tagId);
    this.tagNotes = this.tagNotes.filter(tagNote => tagNote.tagId !== tagId)

    this.localStorageService.saveData('tag', JSON.stringify({tag: this.tags}))
    this.localStorageService.saveData('tag_note', JSON.stringify({tagNote: this.tagNotes}))

    return of(tagId);
  }

  updateTagListOfNote(tagIds: number[], noteId: number): Observable<{tagNotes:{tagId: number, noteId: number}[], tags: Tag[]}> {

    let newTagNotes = this.tagNotes.filter(tagNote => tagNote.noteId !== noteId)
    tagIds.forEach(tagId => {
      let tagNotesExisted = this.tagNotes.filter(tagNote => tagNote.tagId === tagId && tagNote.noteId === noteId)
      if (tagNotesExisted.length > 0) {
        newTagNotes = newTagNotes.concat(tagNotesExisted)
      }
      else {
        newTagNotes.push({
          tagId,
          noteId
        })
      }
    });

    this.tagNotes = newTagNotes

    this.localStorageService.saveData('tag_note', JSON.stringify({tagNote: newTagNotes}))

    // delete free tag
    let freeTag = []

    this.tags.forEach(tag => {
      let tagsInTagNotes = this.tagNotes.filter(tagNote => tagNote.tagId === tag.id)
      if (tagsInTagNotes.length === 0) {
        freeTag.push(tag)
      }
    });

    freeTag.forEach(freeTag => {
      this.tags = this.tags.filter(tag => tag.id !== freeTag.id)
    });

    this.localStorageService.saveData('tag', JSON.stringify({tag: this.tags}))

    let returnObject = {
      tagNotes: newTagNotes,
      tags: this.tags
    }

    return of(returnObject);
  }

  updateSelectedTagId(selectedTagId: number): Observable<number> {
    this.selectedTagId = selectedTagId;
    return of(selectedTagId)
  }
}
