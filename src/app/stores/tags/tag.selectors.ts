import { createFeatureSelector, createSelector } from '@ngrx/store';

import { TagState } from './tag.states';

export const selectTagState = createFeatureSelector<TagState>('tag');
export const selectTagsList = createSelector(selectTagState, (state) => state.tags);
export const selectTagIdsByNoteId = createSelector(selectTagState, (state) => state.tagIdsByNoteId);
export const selectSelectedTagId = createSelector(selectTagState, (state) => state.selectedTagId);
