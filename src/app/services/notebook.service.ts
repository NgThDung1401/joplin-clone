import { Injectable } from '@angular/core';
import { Notebook } from '../models/notebook.model';
import { Observable, of } from 'rxjs';
import { LocalStorageService } from './localStorageService';

@Injectable({
    providedIn: 'root'
})
export class NotebookService {

    private notebooksList: Notebook[] = []

    private selectedNotebookId: number = 0

    constructor(private readonly localStorageService: LocalStorageService) {
      const notebookFromLS = JSON.parse(localStorageService.getData('notebook'))
      if (notebookFromLS && notebookFromLS.notebook) {
        this.notebooksList = notebookFromLS.notebook
      }
    }

    getNotebooks(): Observable<Notebook[]> {
      return of(this.notebooksList);
    }

    getSelectedNotebookId():Observable<number> {
      return of(this.selectedNotebookId)
    }

    updateSelectedNotebookId(selectedNotebookId: number):Observable<number> {
      this.selectedNotebookId = selectedNotebookId;

      return of(this.selectedNotebookId);
    }

    create(notebook: Notebook): Observable<Notebook> {
      let newNotebookId = 1;

      if (this.notebooksList.length > 0) {
        newNotebookId = this.notebooksList[this.notebooksList.length - 1].id + 1
      }

      let newNotebook = {
        id: newNotebookId,
        name: notebook.name
      }

      this.notebooksList = [
          ...this.notebooksList,
          newNotebook
      ];

      this.localStorageService.saveData('notebook', JSON.stringify({notebook: this.notebooksList}))

      return of(newNotebook);
    }

    delete(notebook: Notebook): Observable<Notebook> {
      this.notebooksList = this.notebooksList.filter(b => b.id !== notebook.id);

      this.localStorageService.saveData('notebook', JSON.stringify({notebook: this.notebooksList}))

      return of(notebook);
    }
}
