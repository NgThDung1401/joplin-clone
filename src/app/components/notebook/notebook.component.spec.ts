import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotebookComponent } from './notebook.component';
import { NoteListComponent } from '../note-list/note-list.component';

import { AppModule } from 'src/app/app.module';
import { ActivatedRoute } from '@angular/router';

import { NzIconModule } from 'ng-zorro-antd/icon';
import { IconDefinition } from '@ant-design/icons-angular';
import {TagsOutline, BookOutline, PlusOutline, TagOutline} from '@ant-design/icons-angular/icons';

const icons: IconDefinition[] = [TagsOutline, BookOutline, PlusOutline, TagOutline]
const activatedRouteMock = {
  snapshot: {
    params: {
      id: '1'
    }
  }
}

describe('NotebookComponent', () => {
  let component: NotebookComponent;
  let fixture: ComponentFixture<NotebookComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        AppModule,
        NzIconModule.forChild(icons),
      ],
      declarations: [
        NotebookComponent,
        NoteListComponent,
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: activatedRouteMock,
        }
      ]
    }).compileComponents();
    fixture = TestBed.createComponent(NotebookComponent);
    component = fixture.componentInstance;
    component.selectedNote = {
      id: 1,
      notebookId: 1,
      title: 'Note 1',
      content: 'Note 1',
    }

    component.notesToDisplay = [
      {
        id: 1,
        notebookId: 1,
        title: 'Note 1',
        content: 'Note 1',
      }
    ]
    fixture.detectChanges();
  });

  it('should create the notebook component', () => {
    expect(component).toBeTruthy();
  });
});
