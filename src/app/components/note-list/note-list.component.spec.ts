import { ComponentFixture, TestBed, fakeAsync, flush } from '@angular/core/testing';

import { NoteListComponent } from './note-list.component';

import { AppModule } from 'src/app/app.module';

import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { IconDefinition } from '@ant-design/icons-angular';
import {TagsOutline, BookOutline, PlusOutline, TagOutline} from '@ant-design/icons-angular/icons';
import { By } from '@angular/platform-browser';

const icons: IconDefinition[] = [TagsOutline, BookOutline, PlusOutline, TagOutline]

const mockNotesToDisplay = [
  {
    id: 1,
    notebookId: 1,
    title: 'Note 1',
    content: 'Note 1',
  },
  {
    id: 2,
    notebookId: 1,
    title: 'Note 1_2',
    content: 'Note 1_2',
  },
  {
    id: 3,
    notebookId: 1,
    title: 'Note 1_3',
    content: 'Note 1_3',
  }
]

describe('NoteListComponent', () => {
  let component: NoteListComponent;
  let fixture: ComponentFixture<NoteListComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        AppModule,
        NzIconModule.forChild(icons),
        NzMenuModule,
        NzInputModule,
      ],
      declarations: [
        NoteListComponent,
      ]
    });
    fixture = TestBed.createComponent(NoteListComponent);
    component = fixture.componentInstance;
    component.selectedNote = {
      id: 1,
      notebookId: 1,
      title: 'Note 1',
      content: 'Note 1',
    }
    component.notesToDisplay = mockNotesToDisplay
    fixture.detectChanges();
  });

  it('should create the note list component', () => {
    expect(component).toBeTruthy();
  });

  it('changeSelectedNote() should has been called when note in note list is clicked', fakeAsync(() => {
    component = fixture.componentInstance;
    component.notesToDisplay = mockNotesToDisplay;
    fixture.detectChanges();

    const changeSelectedNoteSpy = spyOn(component, 'changeSelectedNote');
    const noteItems = fixture.debugElement.queryAll(By.css('li.note-item'));

    mockNotesToDisplay.forEach((note, index) => {
      noteItems[index].nativeElement.click();
      flush();
      expect(changeSelectedNoteSpy).toHaveBeenCalledWith(note);
    });
  }))

  it('turnOnCreatingMode() should has been called when "Create note" button is clicked', fakeAsync(() => {
    component = fixture.componentInstance;
    fixture.detectChanges();

    const turnOnCreatingModeSpy = spyOn(component, 'turnOnCreatingMode');
    const createNoteButton = fixture.debugElement.query(By.css('button.new-note-btn'));

    createNoteButton.nativeElement.click();
    flush();
    expect(turnOnCreatingModeSpy).toHaveBeenCalled();
  }))

  it('searchNote() should has been called when "Search" input is changed', fakeAsync(() => {
    component = fixture.componentInstance;
    fixture.detectChanges();

    const searchNoteSpy = spyOn(component, 'searchNote');
    const searchNoteInput = fixture.debugElement.query(By.css('input.search-note-input'));

    searchNoteInput.nativeElement.value = 'note';
    searchNoteInput.nativeElement.dispatchEvent(new Event('input'));
    fixture.detectChanges();

    expect(searchNoteSpy).toHaveBeenCalled();
  }))

  it('clearSearchValue() should has been called when "Clear" icon on "Search" input is clicked', fakeAsync(() => {
    component = fixture.componentInstance;
    component.searchValue = 'note'
    fixture.detectChanges();

    const clearSearchValueSpy = spyOn(component, 'clearSearchValue');
    const clearSearchValueSpan = fixture.debugElement.query(By.css('span.clear-search-value'));

    clearSearchValueSpan.nativeElement.click();
    flush();
    expect(clearSearchValueSpy).toHaveBeenCalled();
  }))
});
