import { Notebook } from "src/app/models/notebook.model";

export interface NotebookState {
  notebooks: Notebook[],
  selectedNotebookId: number
}
