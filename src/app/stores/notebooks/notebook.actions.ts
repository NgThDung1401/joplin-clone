import { createAction, props } from '@ngrx/store';
import { Notebook } from 'src/app/models/notebook.model';

const prefix = '[Notebooks]';

export const getNotebooks = createAction(`${prefix} Get Notebooks`);
export const getNotebooksSuccess = createAction(
  `${getNotebooks.type} Success`,
  props<{
    notebooks: Notebook[];
  }>()
);

export const getSelectedNotebookId = createAction(`${prefix} Get Selected Notebook Id`);
export const getSelectedNotebookIdSuccess = createAction(
  `${getSelectedNotebookId.type} Success`,
  props<{
    selectedNotebookId: number
  }>()
);

export const updateSelectedNotebookId = createAction(
  `${prefix} Update Selected Notebook Id`,
  props<{
    selectedNotebookId: number
  }>()
);

export const updateSelectedNotebookIdSuccess = createAction(
  `${updateSelectedNotebookId.type} Success`,
  props<{
    selectedNotebookId: number
  }>()
);

export const createNotebook = createAction(
  `${prefix} Create Notebook`,
  props<{
    notebook: Notebook
  }>()
);

export const createNotebookSuccess = createAction(
  `${createNotebook.type} Success`,
  props<{
    notebook: Notebook
  }>()
);

export const deleteNotebook = createAction(
  `${prefix} Delete Notebook`,
  props<{
    notebook: Notebook
  }>()
);

export const deleteNotebookSuccess = createAction(
  `${deleteNotebook.type} Success`,
  props<{
    notebook: Notebook
  }>()
);
