import { NgModule } from '@angular/core';

import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { NotebooksEffects } from './notebook.effects';
import { notebookReducer } from './notebook.reducers';

@NgModule({
  imports: [
      StoreModule.forFeature('notebook', notebookReducer),
      EffectsModule.forFeature([NotebooksEffects])
  ]
})
export class NotebookStoreModule {}
