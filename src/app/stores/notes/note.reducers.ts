import { createReducer, on } from '@ngrx/store';

import { NoteState } from './note.state';
import * as noteActions from './note.actions';
import { LocalStorageService } from 'src/app/services/localStorageService';

const localStorageService = new LocalStorageService()
const noteFromLS = JSON.parse(localStorageService.getData('note'))

export const initialNoteState: NoteState = {
  notes: (noteFromLS && noteFromLS.notebook) ? noteFromLS.notebook : [],
  notesToDisplay: (noteFromLS && noteFromLS.notebook) ? noteFromLS.notebook : [],
  selectedNote: (noteFromLS && noteFromLS.notebook && noteFromLS.notebook.length > 0) ? noteFromLS.notebook[0] : {
    id: 0,
    notebookId: 0,
    title: '',
    content: ''
  }
};

const reducer = createReducer<NoteState>(
  initialNoteState,
  on(noteActions.getNotes, (state) => {
    return {
      ...state
    }
  }),
  on(noteActions.getNotesSuccess, (state, { notes }) => {
    return {
      ...state,
      notes
    };
  }),

  on(noteActions.getNotesToDisplay, (state) => {
    return {
      ...state
    }
  }),
  on(noteActions.getNotesToDisplaySuccess, (state, { notesToDisplay }) => {
    return {
      ...state,
      notesToDisplay
    };
  }),

  on(noteActions.getSelectedNotes, (state) => {
    return {
      ...state
    }
  }),
  on(noteActions.getSelectedNotesSuccess, (state, { selectedNote }) => {
    return {
      ...state,
      selectedNote
    };
  }),

  on(noteActions.updateNotesToDisplay, (state) => {
    return {
      ...state
    }
  }),
  on(noteActions.updateNotesToDisplaySuccess, (state, { notesToDisplay }) => {
    return {
      ...state,
      notesToDisplay,
    };
  }),

  on(noteActions.createNote, (state) => {
    return {
      ...state,
    };
  }),
  on(noteActions.createNoteSuccess, (state, { note }) => {
    return {
      ...state,
      notes: [...state.notes, note],
      notesToDisplay: [...state.notesToDisplay, note],
      selectedNote: note
    };
  }),

  on(noteActions.updateSelectedNote, (state) => {
    return {
      ...state,
    };
  }),
  on(noteActions.updateSelectedNoteSuccess, (state, { selectedNote }) => {
    return {
      ...state,
      selectedNote,
    };
  }),

  on(noteActions.updateSelectedNoteById, (state) => {
    return {
      ...state,
    };
  }),
  on(noteActions.updateSelectedNoteByIdSuccess, (state, { selectedNote }) => {
    return {
      ...state,
      selectedNote,
    };
  }),

  on(noteActions.updateNote, (state) => {
    return {
      ...state,
    };
  }),
  on(noteActions.updateNoteSuccess, (state, { updateNote }) => {
    return {
      ...state,
      notes: state.notes.map(note => note.id === updateNote.id ? updateNote : note),
      notesToDisplay: state.notesToDisplay.map(note => note.id === updateNote.id ? updateNote : note),
      selectedNote: updateNote
    };
  }),

  on(noteActions.deleteNote, (state) => {
    return {
      ...state,
    };
  }),
  on(noteActions.deleteNoteSuccess, (state, { deleteNoteId }) => {
    let newSelectedNote = state.selectedNote
    let newNoteToDisplay = state.notesToDisplay.filter(note => note.id !== deleteNoteId)

    if (deleteNoteId === state.selectedNote.id) {
      newSelectedNote = newNoteToDisplay.length > 0 ? newNoteToDisplay[0] : {
        id: 0,
        notebookId: 0,
        title: '',
        content: ''
      }
    }

    return {
      ...state,
      notes: state.notes.filter(note => note.id !== deleteNoteId),
      notesToDisplay: newNoteToDisplay,
      selectedNote:  newSelectedNote
    };
  }),

  on(noteActions.deleteNoteByNotebookId, (state) => {
    return {
      ...state,
    };
  }),
  on(noteActions.deleteNoteByNotebookIdSuccess, (state, { deleteNotebookId }) => {
    let newNotes = state.notes.filter(note => note.notebookId !== deleteNotebookId)

    return {
      ...state,
      notes: newNotes,
    };
  }),
)

export function noteReducer(state = initialNoteState, actions: any): NoteState {
  return reducer(state, actions)
}
