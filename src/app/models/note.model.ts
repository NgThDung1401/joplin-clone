export interface Note {
  id: number,
  notebookId: number,
  title: string,
  content?: string,
  createdAt?: number,
  updatedAt?: number,
}
