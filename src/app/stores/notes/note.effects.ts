import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, switchMap } from 'rxjs/operators';
import * as noteActions from './note.actions';
import { NoteService } from 'src/app/services/note.service';
import { Note } from 'src/app/models/note.model';

@Injectable()
export class NotesEffects {
  constructor(private readonly actions$: Actions, private readonly noteService: NoteService) {}

  getNotes$ = createEffect(() =>
    this.actions$.pipe(
      ofType(noteActions.getNotes.type),
      switchMap(() => this.noteService.getNotes()),
      map((notes: Note[]) => noteActions.getNotesSuccess({notes}))
    )
  );

  getNotesToDisplay$ = createEffect(() =>
    this.actions$.pipe(
      ofType(noteActions.getNotesToDisplay.type),
      switchMap(() => this.noteService.getNotesToDisplay()),
      map((notesToDisplay: Note[]) => noteActions.getNotesToDisplaySuccess({notesToDisplay}))
    )
  );

  getSeletedNote$ = createEffect(() =>
    this.actions$.pipe(
      ofType(noteActions.getSelectedNotes.type),
      switchMap(() => this.noteService.getSelectedNote()),
      map((selectedNote: Note) => noteActions.getSelectedNotesSuccess({selectedNote}))
    )
  );

  updateNotesToDisplay$ = createEffect(() =>
    this.actions$.pipe(
      ofType(noteActions.updateNotesToDisplay),
      switchMap(({itemId, itemType}) => this.noteService.updateNotesToDisplay(itemId, itemType)),
      map((notesToDisplay: Note[]) => noteActions.updateNotesToDisplaySuccess({notesToDisplay}))
    )
  );

  createNote$ = createEffect(() =>
    this.actions$.pipe(
      ofType(noteActions.createNote),
      switchMap(({note}) => this.noteService.create(note)),
      map((note: Note) => noteActions.createNoteSuccess({note}))
    )
  );

  updateSelectedNoteId$ = createEffect(() =>
    this.actions$.pipe(
      ofType(noteActions.updateSelectedNote),
      switchMap(({selectedNote}) => this.noteService.updateSelectedNote(selectedNote)),
      map((selectedNote: Note) => noteActions.updateSelectedNoteSuccess({selectedNote}))
    )
  );

  updateSelectedNoteById$ = createEffect(() =>
    this.actions$.pipe(
      ofType(noteActions.updateSelectedNoteById),
      switchMap(({noteId}) => this.noteService.updateSelectedNoteById(noteId)),
      map((selectedNote: Note) => noteActions.updateSelectedNoteByIdSuccess({selectedNote}))
    )
  );

  updateNote$ = createEffect(() =>
    this.actions$.pipe(
      ofType(noteActions.updateNote),
      switchMap(({updateNote}) => this.noteService.update(updateNote)),
      map((updateNote: Note) => noteActions.updateNoteSuccess({updateNote}))
    )
  );

  deleteNote$ = createEffect(() =>
    this.actions$.pipe(
      ofType(noteActions.deleteNote),
      switchMap(({deleteNoteId}) => this.noteService.delete(deleteNoteId)),
      map((deleteNoteId: number) => noteActions.deleteNoteSuccess({deleteNoteId}))
    )
  );

  deleteNoteByNotebookId$ = createEffect(() =>
    this.actions$.pipe(
      ofType(noteActions.deleteNoteByNotebookId),
      switchMap(({deleteNotebookId}) => this.noteService.deleteByNotebookId(deleteNotebookId)),
      map((deleteNotebookId: number) => noteActions.deleteNoteByNotebookIdSuccess({deleteNotebookId}))
    )
  );
}
