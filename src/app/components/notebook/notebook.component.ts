import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import * as noteActions from 'src/app/stores/notes/note.actions'
import * as noteSelectors from 'src/app/stores/notes/note.selectors'
import { Note } from 'src/app/models/note.model';

@Component({
  selector: 'app-notebook',
  templateUrl: './notebook.component.html',
  styleUrls: ['./notebook.component.css']
})
export class NotebookComponent implements OnInit{
  selectedNotebookId: number = 0
  selectedTagId: number = 0
  notes: Note[]
  notesToDisplay: Note[]
  selectedNote: Note;
  hasNoteId: boolean = false
  isNotebook: boolean = true

  constructor(private readonly store: Store, private route: ActivatedRoute, private router: Router) {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        const eventUrlSplited = event.url.split('/');

        if (eventUrlSplited[1] === 'notebook') {
          const selectedNotebookId = Number(eventUrlSplited[2])
          if (selectedNotebookId || selectedNotebookId === 0) {
            this.selectedNotebookId = selectedNotebookId
          }
        } else {
          const selectedTagId = Number(eventUrlSplited[2])
          if (selectedTagId || selectedTagId === 0) {
            this.selectedTagId = selectedTagId
            this.isNotebook = false
          }
        }

        const selectedNoteId = Number(eventUrlSplited[4])
        this.hasNoteId = (selectedNoteId) ? true : false

        this.handleRouting();
      }
    });
  }

  ngOnInit(): void {
    this.selectedNotebookId = Number(this.route.snapshot.params['id']);
    this.initDispatch();
    this.initSubscriptions();
    this.handleRouting();
  }

  private initDispatch(): void {
    this.store.dispatch(noteActions.getNotes());
    this.store.dispatch(noteActions.getNotesToDisplay());
    this.store.dispatch(noteActions.getSelectedNotes());
  }

  private initSubscriptions(): void {
    this.store.pipe(select(noteSelectors.selectNotesList)).subscribe(value => {
      this.notes = value
    });

    this.store.pipe(select(noteSelectors.selectNotesToDisplayList)).subscribe(value => {
      this.notesToDisplay = value
    });

    this.store.pipe(select(noteSelectors.selectSelectedNote)).subscribe(value => {
      this.selectedNote = value
    })
  }

  private handleRouting() {
    const prefix = this.isNotebook ? 'notebook' : 'tag';
    if (this.isNotebook) {
      this.store.dispatch(noteActions.updateNotesToDisplay({itemId: this.selectedNotebookId, itemType: 'notebook'}))
    }else {
      this.store.dispatch(noteActions.updateNotesToDisplay({itemId: this.selectedTagId, itemType: 'tag'}))
    }
    if (!this.hasNoteId && (this.notesToDisplay && this.notesToDisplay.length < 1)) {
      this.router.navigate([`/${prefix}/${this.selectedNotebookId}/note/0`]);
    } else if (!this.hasNoteId && (this.notesToDisplay && this.notesToDisplay.length > 0)) {
      this.router.navigate([`/${prefix}/${this.selectedNotebookId}/note/${this.notesToDisplay[0].id}`]);
    }
  }

  private getCompareText(text: string): string {
    return text.toLocaleLowerCase().replace(/\s/g, '');
  }

  public searchNote(searchValue: string) {
    if (searchValue !== '') {
      this.notesToDisplay = this.notes.filter(note => this.getCompareText(note.title).includes(this.getCompareText(searchValue)));
    } else {
      this.store.dispatch(noteActions.updateNotesToDisplay({itemId: this.selectedNotebookId, itemType: 'notebook'}))
    }
  }
}
