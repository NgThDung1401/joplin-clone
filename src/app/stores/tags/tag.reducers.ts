import { createReducer, on } from '@ngrx/store';

import { TagState } from './tag.states';
import * as tagActions from './tag.actions';
import { LocalStorageService } from 'src/app/services/localStorageService';

const localStorageService = new LocalStorageService()
const tagFromLS = JSON.parse(localStorageService.getData('tag'))
const tagNoteFromLS = JSON.parse(localStorageService.getData('tag_note'))

export const initialTagState: TagState = {
  tags: (tagFromLS && tagFromLS.tag) ? tagFromLS.tag : [],
  tagIdsByNoteId: [],
  selectedTagId: 0
};

const reducer = createReducer<TagState>(
  initialTagState,
  on(tagActions.getTags, (state) => {
    return {
      ...state
    }
  }),
  on(tagActions.getTagsSuccess, (state, { tags }) => {
    return {
      ...state,
      tags
    };
  }),

  on(tagActions.getTagIdsByNoteId, (state) => {
    return {
      ...state
    }
  }),
  on(tagActions.getTagIdsByNoteIdSuccess, (state, { tagIds }) => {
    return {
      ...state,
      tagIdsByNoteId: tagIds
    };
  }),

  on(tagActions.createTag, (state) => {
    return {
      ...state,
    };
  }),
  on(tagActions.createTagSuccess, (state, { tag }) => {
    return {
      ...state,
      tags: [...state.tags, tag],
    };
  }),

  on(tagActions.deleteTag, (state) => {
    return {
      ...state,
    };
  }),
  on(tagActions.deleteTagSuccess, (state, { deleteTagId }) => {
    return {
      ...state,
      tags: state.tags.filter((tag) => tag.id !== deleteTagId),
      tagIdsByNoteId: state.tagIdsByNoteId.filter((tagId) => tagId !== deleteTagId),
    };
  }),

  on(tagActions.updateTagListOfNote, (state) => {
    return {
      ...state,
    };
  }),
  on(tagActions.updateTagListOfNoteSuccess, (state, returnObject) => {
    return {
      ...state,
      tags: returnObject.tags
    };
  }),

  on(tagActions.updateSelectedTagId, (state) => {
    return {
      ...state,
    };
  }),
  on(tagActions.updateSelectedTagIdSuccess, (state, { selectedTagId }) => {
    return {
      ...state,
      selectedTagId
    };
  }),
)

export function tagReducer(state = initialTagState, actions: any): TagState {
  return reducer(state, actions)
}
