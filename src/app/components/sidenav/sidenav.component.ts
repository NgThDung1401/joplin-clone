import { Component, OnInit } from '@angular/core';
import { Notebook } from 'src/app/models/notebook.model';
import { select, Store } from '@ngrx/store';
import * as notebookActions from 'src/app/stores/notebooks/notebook.actions'
import * as notebookSelectors from 'src/app/stores/notebooks/notebook.selectors'
import * as noteSelectors from 'src/app/stores/notes/note.selectors'
import * as noteActions from 'src/app/stores/notes/note.actions'
import * as tagSelectors from 'src/app/stores/tags/tag.selectors'
import * as tagActions from 'src/app/stores/tags/tag.actions'

import { NzModalService } from 'ng-zorro-antd/modal';
import { NzContextMenuService, NzDropdownMenuComponent } from 'ng-zorro-antd/dropdown';
import { Tag } from 'src/app/models/tag.model';

import { Router, NavigationEnd } from '@angular/router';
import { Note } from 'src/app/models/note.model';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {
  notebooks: Notebook[];
  selectedNotebookId: number = 0;
  isVisible:boolean = false;
  newNotebookName: string = "";
  notesToDisplay: Note[]

  tags: Tag[];
  selectedTagId: number = 0;

  ngOnInit(): void {
    this.initDispatch();
    this.initSubscriptions();
  }

  constructor(
    private readonly store: Store,
    private modal: NzModalService,
    private nzContextMenuService: NzContextMenuService,
    private router: Router) {
      this.router.events.subscribe(event => {
        if (event instanceof NavigationEnd) {
          const eventUrlSplited = event.url.split('/')
          if (eventUrlSplited[1] === 'notebook') {
            this.selectedNotebookId = Number(eventUrlSplited[2]);
            this.store.dispatch(notebookActions.updateSelectedNotebookId({selectedNotebookId: this.selectedNotebookId}))
            this.store.dispatch(tagActions.updateSelectedTagId({selectedTagId: 0}))
            this.store.dispatch(noteActions.updateNotesToDisplay({itemId: this.selectedNotebookId, itemType: 'notebook'}))
          } else if (eventUrlSplited[1] === 'tag') {
            this.selectedTagId = Number(eventUrlSplited[2]);
            this.store.dispatch(tagActions.updateSelectedTagId({selectedTagId: this.selectedTagId}));

          }
        }
      });
  }

  private initDispatch(): void {
    this.store.dispatch(notebookActions.getNotebooks());
    this.store.dispatch(notebookActions.getSelectedNotebookId());
  }

  private initSubscriptions(): void {
    this.store
    .pipe(select(notebookSelectors.selectNotebooksList))
    .subscribe(value => this.notebooks = value);

    this.store
    .pipe(select(notebookSelectors.selectSelectedNotebookId))
    .subscribe(value => this.selectedNotebookId = value);

    this.store
    .pipe(select(tagSelectors.selectTagsList))
    .subscribe(value => this.tags = value);

    this.store
    .pipe(select(tagSelectors.selectSelectedTagId))
    .subscribe(value => this.selectedTagId = value);

    this.store
    .pipe(select(noteSelectors.selectNotesToDisplayList))
    .subscribe(value => this.notesToDisplay = value);
  }

  public changeSelectedNotebookId(selectedNotebookId: number): void {
    this.store.dispatch(notebookActions.updateSelectedNotebookId({selectedNotebookId}))
    this.store.dispatch(tagActions.updateSelectedTagId({selectedTagId: 0}))
    this.store.dispatch(noteActions.updateNotesToDisplay({itemId: selectedNotebookId, itemType: 'notebook'}))
    if (this.notesToDisplay.length > 0) {
      this.router.navigate([`/notebook/${selectedNotebookId}/note/${this.notesToDisplay[0].id}`]);
    } else {
      this.router.navigate([`/notebook/${selectedNotebookId}/note/0`]);
    }
  }

  public changeSelectedTagId(selectedTagId: number) {
    this.store.dispatch(tagActions.updateSelectedTagId({selectedTagId}))
    this.store.dispatch(notebookActions.updateSelectedNotebookId({selectedNotebookId: 0}))
    this.store.dispatch(noteActions.updateNotesToDisplay({itemId: selectedTagId, itemType: 'tag'}))
    this.router.navigate([`/tag/${selectedTagId}/note/${this.notesToDisplay[0].id}`]);
  }

  public showModal(): void {
    this.isVisible = true;
  }

  public handleOk(): void {
    this.isVisible = false;
    let newNotebook = {
      id: 1,
      name: this.newNotebookName
    }
    this.store.dispatch(notebookActions.createNotebook({notebook: newNotebook}))
    this.newNotebookName = "";
  }

  public handleCancel(): void {
    this.isVisible = false;
    this.newNotebookName = "";
  }

  public showConfirm(notebook: Notebook) {
    this.modal.confirm({
      nzTitle: `Delete notebook "${notebook.name}"?`,
      nzContent: 'All notes within notebook will also be deleted.',
      nzOkText: 'OK',
      nzOnOk: () => this.deleteNotebook(notebook.id),
      nzCancelText: 'Cancel',
      nzOnCancel: () => {}
    });
  }

  public showConfirmDeleteTag(tag: Tag) {
    this.modal.confirm({
      nzTitle: `Delete tag "${tag.label}"?`,
      nzContent: '',
      nzOkText: 'OK',
      nzOnOk: () => this.deleteTag(tag.id),
      nzCancelText: 'Cancel',
      nzOnCancel: () => {}
    });
  }

  public deleteNotebook(id: number) {
    let deleteNotebook = {
      id: id,
      name: ''
    }
    this.store.dispatch(notebookActions.deleteNotebook({notebook: deleteNotebook}))
    this.store.dispatch(noteActions.deleteNoteByNotebookId({deleteNotebookId: id}))
    if (id === this.selectedNotebookId) {
      this.store.dispatch(notebookActions.updateSelectedNotebookId({selectedNotebookId: 0}))
      this.store.dispatch(noteActions.updateNotesToDisplay({itemId: 0, itemType: 'notebook'}))
    } else {
      this.store.dispatch(noteActions.updateNotesToDisplay({itemId: this.selectedNotebookId, itemType: 'notebook'}))
    }
  }

  public contextMenu($event: MouseEvent, menu: NzDropdownMenuComponent) {
    this.nzContextMenuService.create($event, menu);
  }

  public deleteTag(id: number) {
    this.store.dispatch(tagActions.deleteTag({deleteTagId: id}));
  }

  public contextTagMenu($event: MouseEvent, tagMenu: NzDropdownMenuComponent) {
    this.nzContextMenuService.create($event, tagMenu);
  }
}
